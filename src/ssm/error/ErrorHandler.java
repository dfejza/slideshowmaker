package ssm.error;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & Dardan Fejza
 */
public class ErrorHandler {
    // APP UI
    private SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(LanguagePropertyType errorType, String errorDialogTitle, String errorDialogMessage)
    {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorFeedbackText = props.getProperty(errorType);


        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        Stage stage = new Stage();
        stage.initStyle(StageStyle.UTILITY);
        stage.setTitle(errorDialogTitle);
        
        Text text = new Text(10, 40, errorDialogMessage);
        text.setWrappingWidth(380);
        Group group = new Group(text);

        Scene scene = new Scene(group,420,120);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        group.getStyleClass().add("error_dialogue");
        text.getStyleClass().add("text");

        
        stage.setScene(scene);
        stage.showAndWait();
    }    
    
     /**
     * Similar to other processError, accepts languageTypes rather than strings
     * 
     * @param message Message, detailing the error occured.
     */
    public void processError(LanguagePropertyType title, LanguagePropertyType message)
    {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        String errorDialogTitle = props.getProperty(title);
        String errorDialogMessage = props.getProperty(message);
             
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        Stage stage = new Stage();
        stage.initStyle(StageStyle.UTILITY);
        stage.setTitle(errorDialogTitle);
        
        BorderPane pane = new BorderPane();
        Text text = new Text(10,40,errorDialogMessage);
        text.setWrappingWidth(380);
        pane.setCenter(text);

        Scene scene = new Scene(pane,420,120);
        
        scene.getStylesheets().add(STYLE_SHEET_UI);
        pane.getStyleClass().add("error_dialogue");
        text.getStyleClass().add("text");

        
        stage.setScene(scene);
        stage.showAndWait();
    }   
}
