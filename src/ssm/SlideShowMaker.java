package ssm;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_ENG;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_JPN;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Dardan Fejza
 */
public class SlideShowMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);
    
    @Override
    public void start(Stage primaryStage) throws Exception {     
        
        
            //Create a new stage, allowing user to select language
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            stage.setTitle("Language Select");
            ObservableList<String> options = 
            FXCollections.observableArrayList(
                "English",
                "日本語"
            );
            final ComboBox comboBox = new ComboBox(options);
            comboBox.setValue("English");
            BorderPane pane = new BorderPane();

            FlowPane northSouthPane = new FlowPane();
            northSouthPane.setAlignment(Pos.CENTER);
            Button buttonOk = new Button("Ok");
            Button buttonCancel = new Button("Cancel");
            buttonOk.setOnAction(e -> {
                //Poll what is in the combo box and use that language
                System.out.println(comboBox.getValue());
                //comboBox.getValue();
                stage.hide();
            });
            buttonCancel.setOnAction(e -> {
                // Cancel key to terminate program
                stage.hide();
                System.exit(0);
            });
            northSouthPane.getChildren().addAll(buttonOk, buttonCancel);     
            pane.setCenter(comboBox);
            pane.setBottom(northSouthPane);
            Scene scene = new Scene(pane, 150, 100);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            pane.getStyleClass().add("slidesEditorPane");
            stage.setScene(scene);
            stage.showAndWait();
            
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        String selected = (String) comboBox.getValue();
        boolean success = loadProperties(selected);
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "Language Error", "Error loading the selected language XML file.");
	    System.exit(0);
	}
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties(String selected) {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            if(selected.equals("English"))
                props.loadProperties(UI_PROPERTIES_FILE_NAME_ENG, PROPERTIES_SCHEMA_FILE_NAME);
            else
                props.loadProperties(UI_PROPERTIES_FILE_NAME_JPN, PROPERTIES_SCHEMA_FILE_NAME);
                        
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
           
            // COMMENTING OUT FOR THE TIME BEING. SEEMS REDUNDENT TO INCLUDE THE ERROR HANDLE TWICE
            //ErrorHandler eH = ui.getErrorHandler();
            //eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "todo", "todo");
            return false;
        }        
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
    
}
