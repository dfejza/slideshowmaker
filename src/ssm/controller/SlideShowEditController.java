package ssm.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.file.Files;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.LanguagePropertyType.ERROR_UNABLE_TO_LOAD_IMAGE;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.error.ErrorHandler;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;


/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 *
 * @author McKilla Gorilla & Dardan Fejza
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    // Images
    ImageView imageSelectionView;
    
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
        ui = initUI;
    }
    
    
    public void handleViewSlideShowRequest() throws FileNotFoundException, UnsupportedEncodingException {
        SlideShowModel slideShow = ui.getSlideShow();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ObservableList<Slide> slides = slideShow.getSlides();
        
        // GENERATE SITE DIRECTORY
        File dir = new File("./Site");
        if (!dir.exists()) {
            dir.mkdir();
        }
        
        // GENERATE SLIDESHOW TITLE DIRECTORY
        String TITLE = slideShow.getTitle();
        dir = new File("./Site/" + TITLE);
        if (!dir.exists()) {
            dir.mkdir();
        }
        
        // GENERATE CSS DIRECTORY
        dir = new File("./Site/" + TITLE + "/css");
        if (!dir.exists()) {
            dir.mkdir();
        }
        
        // GENERATE js DIRECTORY
        dir = new File("./Site/" + TITLE + "/js");
        if (!dir.exists()) {
            dir.mkdir();
        }
        // GENERATE image DIRECTORY
        dir = new File("./Site/" + TITLE + "/img");
        if (!dir.exists()) {
            dir.mkdir();
        }
        
        // Create metadata containing info about slides
        createMetaData();
        
        // Copy model of CSS,JS and INDEX.html to site directory
        copyFiles("./src/ssm/model/Slideshow.js", "./Site/" + TITLE + "/js/Slideshow.js");
        copyFiles("./src/ssm/model/slideshow_style.css", "./Site/" + TITLE + "/css/slideshow_style.css");
        copyFiles("./src/ssm/model/index.html", "./Site/" + TITLE + "/index.html");
        
        // Copy the images to the SITE/IMAGES directory
        for(Slide tab : slideShow.getSlides()){
            copyFiles((tab.getImagePath() + "/" + tab.getImageFileName()) , ("./Site/" + TITLE + "/img/" + tab.getImageFileName()));
        }
        
        // Ensure an empty presentation cant be created.
        if(slides.size()>0){
            
            //Create a new stage, allowing user to select language
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            stage.setTitle("Slideshow");
            
            // GET THE SIZE OF THE SCREEN
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();
            
            // AND USE IT TO SIZE THE WINDOW
            stage.setX(bounds.getMinX());
            stage.setY(bounds.getMinY());
            stage.setWidth(bounds.getWidth());
            stage.setHeight(bounds.getHeight());
            
            StackPane pane = new StackPane();
            
            String path = System.getProperty("user.dir");
            path.replace("\\\\", "/");
            path +=  "/Site/" + TITLE + "/index.html";
            
            WebView view  = new WebView();
            WebEngine webEngine = view.getEngine();
            webEngine.load("file:///" + path);
            pane.getChildren().add(view);
            
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            
            // Clear cache
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    pane.getChildren().remove(view);
                    java.net.CookieManager manager = new java.net.CookieManager();
                    java.net.CookieHandler.setDefault(manager);
                    manager.getCookieStore().removeAll();
                    webEngine.load("about:blank");
                    view.getEngine().loadContent("about:blank");
                    webEngine.reload();
                    java.net.CookieHandler.setDefault(new java.net.CookieManager());
                }
            });
            
            stage.showAndWait();
        }
    }
    
    public void copyFiles(String source, String target){
        File fSource = new File(source);
        File fTarget = new File(target);
        
        if(fTarget.exists())
            fTarget.delete();
        
        try {
            Files.copy(fSource.toPath(), fTarget.toPath());
        } catch (IOException e) {
            
        }
    }
    
    public void createMetaData() throws FileNotFoundException, UnsupportedEncodingException{
        SlideShowModel slideShow = ui.getSlideShow();
        ObservableList<Slide> slides = slideShow.getSlides();
        int counter = 0;
        
        File fTarget = new File("./Site/" + slideShow.getTitle() + "/js/variables.js");
        if(fTarget.exists())
            fTarget.delete();
        
        PrintWriter writer = new PrintWriter("./Site/" + slideShow.getTitle() + "/js/variables.js", "UTF-8");
        writer.println("var title = \"" + slideShow.getTitle() + "\""+";");
        /* MAKES OBJECTS
        for(Slide tab : slideShow.getSlides()){
        writer.println("var slide" + Integer.toString(counter) + " = {");
        writer.println("\tcaption :  \"" + tab.getcaption() + "\",");
        writer.println("\timage :  \"" + tab.getImageFileName() + "\"");
        writer.println("};");
        counter++;
        }*/
        writer.println("var caption = [];");
        writer.println("var image = [];");
        for(Slide tab : slideShow.getSlides()){
            writer.println("caption["+ Integer.toString(counter) +"] = \"" + tab.getcaption() + "\""+";");
            writer.println("image["+ Integer.toString(counter) +"] = \"./img/" + tab.getImageFileName() + "\""+";");
            counter++;
        }
        writer.println("var imageCount = " + Integer.toString(--counter)+";");
        writer.close();
        
    }
    
    
    public void updateSlideImage(Slide slide) {
        String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();
            
            // Resize if greater than screen height
            if(slideImage.getHeight()+200>=bounds.getHeight())
                imageSelectionView.setFitHeight(bounds.getHeight()-400);
            
        } catch (Exception e) {
            ErrorHandler eH = ui.getErrorHandler();
            ErrorHandler errorHandler = ui.getErrorHandler();
            errorHandler.processError(ERROR_TITLE, ERROR_UNABLE_TO_LOAD_IMAGE);
        }
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String DEFAULT_CAPTION = props.getProperty(LanguagePropertyType.DEFAULT_IMAGE_CAPTION);
        slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES,DEFAULT_CAPTION);
    }
    public void processRemoveSlideRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.removeSlide();
    }
    public void processMoveSlideUp() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.slideUp();
    }
    
    public void processMoveSlideDown() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.slideDown();
    }
}
