package ssm.model;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Dardan Fejza
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath, String defaultCaption) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption = defaultCaption;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getcaption() { return caption; }
    
    public boolean equals(Slide two){
        if(caption.equals(two.getcaption()))
            if(imageFileName.equals(two.getImageFileName()))
                return true;
        
    return false;
    }
            
            
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setcaption(String initCaption) {
	caption = initCaption;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
}
