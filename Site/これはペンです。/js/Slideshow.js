var i = 0; 
var auto = 0;
 
function init(){ 
    document.getElementById("slideShowTitle").innerHTML=title; 
    document.getElementById("caption").innerHTML=caption[i]; 
    document.getElementById("slide").src= image[i];  
    
}

function nextImage(){ 
    i++;
    if(i > imageCount ) {
        i = 0; 
    }  
    
    document.getElementById("caption").innerHTML=caption[i]; 
    document.getElementById("slide").src= image[i];  
} 

function autoSS(){ 
    if(auto == 0){
        auto=1;
        document.getElementById("theglyphicon").className ="glyphicon glyphicon-stop";
    }else{
        auto = 0;
        document.getElementById("theglyphicon").className ="glyphicon glyphicon-play-circle";
    }
} 

function previousImage(){ 
    i--;
    if(i < 0 ) {
        i = imageCount; 
    }  
    
    document.getElementById("caption").innerHTML=caption[i]; 
    document.getElementById("slide").src= image[i];  
} 

function checkIfAuto(){
    if(auto==1){
        nextImage();
    }
}